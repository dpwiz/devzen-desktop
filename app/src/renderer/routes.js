export default [
  {
    name: 'root',
    path: '/',
    component: require('components/Layout'),
    redirect: '/podcast',
    children: [
      {
        path: 'podcast',
        component: require('components/Podcast/List')
      },
      {
        path: 'episode/:eid',
        name: 'episode',
        component: require('components/Podcast/Episode')
      },
      {
        path: 'themes/:eid',
        name: 'themes',
        component: require('components/Podcast/Themes')
      },
      {
        path: 'live',
        name: 'live',
        component: require('components/Live')
      },
      {
        path: 'patreon',
        name: 'patreon',
        component: require('components/Patreon')
      }
    ]
  },
  {
    path: '*',
    redirect: '/'
  }
]
